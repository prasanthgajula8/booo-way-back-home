﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Onboarding : MonoBehaviour
{
    public bool MoveRight;
    public bool Jump1;
    public bool Collectible;
    public bool IsJumped;

    public GameObject moveLeft;
    public GameObject moveRight;
    public GameObject SayBooo;
    public GameObject collectiblePopUp;
    public GameObject sensitivityPopUp;

    [HideInInspector]
    public GameObject PopUp;
    public GameObject LeftButton;
    public GameObject RightButton;

    // Use this for initialization
    void Start()
    {
        SayBooo.SetActive(true);

        PopUp = GameObject.FindWithTag("TutorialPopUp");
        ChangeAlphaToMax(PopUp.GetComponent<SpriteRenderer>());

        RightButton.SetActive(false);
        LeftButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player")
        {
            if(IsJumped)
            {
                ChangeAlphaToZero(PopUp.GetComponent<SpriteRenderer>());
                moveLeft.SetActive(true);
                LeftButton.SetActive(true);
                this.GetComponent<BoxCollider2D>().enabled = false;
            }

            if (MoveRight)
            {
                RightButton.SetActive(true);
                LeftButton.SetActive(false);
                moveLeft.SetActive(false);
                moveRight.SetActive(true);
                this.GetComponent<BoxCollider2D>().enabled = false;
            }

            if (Jump1)
            {
                moveRight.SetActive(false);
                LeftButton.SetActive(true);
                sensitivityPopUp.SetActive(true);
                this.GetComponent<BoxCollider2D>().enabled = false;
            }

            if (Collectible)
            {
                sensitivityPopUp.SetActive(false);
                collectiblePopUp.SetActive(true);
                this.GetComponent<BoxCollider2D>().enabled = false;
            }      
        }
    }

    void ChangeAlphaToZero(SpriteRenderer sr)
    {
        Color clr = new Color();
        clr.r = 255;
        clr.g = 255;
        clr.b = 255;
        clr.a = 0;
        sr.color = clr;

    }

    void ChangeAlphaToMax(SpriteRenderer sr)
    {
        Color clr = new Color();
        clr.r = 255;
        clr.g = 255;
        clr.b = 255;
        clr.a = 1;
        sr.color = clr;
    }
}
