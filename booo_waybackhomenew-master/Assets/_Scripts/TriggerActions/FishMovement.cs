﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishMovement : MonoBehaviour
{
    [Tooltip("if IsLeft at start")]
    public bool IsLeft;
    [Tooltip("Left target to move")]
    public GameObject LeftTarget;
    [Tooltip("Right target to move")]
    public GameObject RightTarget;
    //fish can move only if it is true
    [HideInInspector]
    public bool CanMove = false;
    [Tooltip("Speed of Fish")]
    public float MovementSpeed = 5;

    float X_Scale;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(CanMove)
        {
            if(IsLeft)
            {
                if (transform.position != LeftTarget.transform.position)
                {
                    X_Scale = -Mathf.Abs(transform.localScale.x);
                    transform.localScale = new Vector3(X_Scale, transform.localScale.y, transform.localScale.z);

                    transform.position = Vector3.MoveTowards(transform.position, LeftTarget.transform.position, MovementSpeed * Time.deltaTime);
                }
                else
                    CanMove = false;
            }

            if (!IsLeft )
            {
                if (transform.position != RightTarget.transform.position)
                {
                    X_Scale = Mathf.Abs(transform.localScale.x);
                    transform.localScale = new Vector3(X_Scale, transform.localScale.y, transform.localScale.z);

                    transform.position = Vector3.MoveTowards(transform.position, RightTarget.transform.position, MovementSpeed * Time.deltaTime);
                }
                else
                    CanMove = false;
            }
        }
    }
    /// <summary>
    /// turns fish transform towards LeftTarget and sets CanMove to true
    /// </summary>
    public void MoveLeft()
    {
      
        IsLeft = true;
        CanMove = true;

    }
    /// <summary>
    /// turns fish transform towards RightTarget and sets CanMove to true
    /// </summary>
    public void MoveRight()
    {
       
        IsLeft = false;
        CanMove = true;
    }
}
