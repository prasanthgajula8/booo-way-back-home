﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDoorLift : MonoBehaviour {
    public Animator DoorLif;
    public GameObject Door;
    public Animator lever;
    public GameObject Lvr;

    // Use this for initialization
    void Start()
    {
        DoorLif = Door.GetComponent<Animator>();
        lever =Lvr.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            lever.SetTrigger("LeverStart");
            DoorLif.SetTrigger("StartUp");
        }
    }
}
