﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverGravity : MonoBehaviour {

    public GameObject GravitySwitch;
    public Animator Lever;
    public GameObject ForwardGravity1;
    //public GameObject FowardGravity2;
    public GameObject ReverseGravity1;
    //public GameObject ReverseGravity2;

    [Range(-180, 180)]
    public float SwitchedAngle;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Lever.SetTrigger("LeverStart");
            GravitySwitch.GetComponent<GravitySwitch>().TargetAngle = this.SwitchedAngle;
            ForwardGravity1.SetActive(false);
            ReverseGravity1.SetActive(true);
        }
    }
}
