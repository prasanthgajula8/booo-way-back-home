﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roller2Script : MonoBehaviour
{
    int turnSpeed;
    public int tempSpeed;
    // Use this for initialization
    void Start()
    {
        turnSpeed = tempSpeed;
    }

    // Update is called once per frame
    void Update()
    {
            this.transform.Rotate(Vector3.forward, turnSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="RolCol")
        {
            if (turnSpeed == tempSpeed)
            {
                turnSpeed = -tempSpeed;
            }
            else if (turnSpeed == -tempSpeed)
            {
                turnSpeed = tempSpeed;
            }
        }
    }
}
