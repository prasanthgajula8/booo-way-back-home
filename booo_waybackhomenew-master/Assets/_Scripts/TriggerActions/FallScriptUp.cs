﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallScriptUp : MonoBehaviour
{
    bool isCollided;
    int movementSpeed = 10;
    float timer = 0f;
    // Use this for initialization
    void Start()
    {
        isCollided = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isCollided == true)
        {
            timer += Time.deltaTime;
        }
        if (timer >= 2f)
        {
            this.transform.Translate(Vector2.up * movementSpeed * Time.deltaTime);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isCollided = true;
        }
    }
}
