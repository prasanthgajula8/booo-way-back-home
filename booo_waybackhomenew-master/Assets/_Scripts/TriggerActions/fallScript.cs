﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class fallScript : MonoBehaviour
{
    bool isCollided;
    public int movementSpeed = 10;
    float timer = 0f;
    public float FallAfter;
    Vector3 DefaultPosition;

    // Use this for initialization
    void Start()
    {
        //FallAfter = 2f;
        isCollided = false;
        DefaultPosition = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (isCollided == true)
        {
            timer += Time.deltaTime;
            if (timer >= FallAfter)
            {
                this.transform.Translate(Vector2.down * movementSpeed * Time.deltaTime);
            }
        }

        //if (Character.isRespawned)
        //{
        //    movementSpeed = 0;
        //    this.transform.position = DefaultPosition;
        //    Character.isRespawned = false;
        //}
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isCollided = true;
        }
    }
}
