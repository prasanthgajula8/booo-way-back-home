﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverWaterScript : MonoBehaviour
{
    bool isTrigger;
    public GameObject water;
    int movementSpeed=10;
    float timer=0f;
    // Use this for initialization
    void Start()
    {
        isTrigger = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(isTrigger==true)
        {
            timer += Time.deltaTime;
            water.transform.Translate(Vector2.down * movementSpeed * Time.deltaTime);
        }
        if(timer>=4f)
        {
            movementSpeed = 0;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player")
        {
            water = GameObject.Find("MyWater");
            isTrigger = true;
        }
    }
}
