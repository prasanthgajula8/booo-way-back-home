﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class rightPtRotateScript : MonoBehaviour
{
    public int turnSpeed = 10;
    public GameObject rotatingPlatform;
    [HideInInspector]
    public bool isTriggered;
    float turnIncrement = 0.3f;
    float tempSpeed;

    // Use this for initialization
    void Start()
    {
        tempSpeed = turnSpeed;
        isTriggered = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isTriggered == true)
        {
            tempSpeed += turnIncrement;
            rotatingPlatform.transform.Rotate(Vector3.forward, -tempSpeed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isTriggered = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isTriggered = false;
            tempSpeed = 0;
        }
    }
}
