﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterDown : MonoBehaviour
{
    bool CanMove;
    public GameObject Water;
    public int MovementSpeed;
    public float distanceToBeMoved;
    float y;
    bool alreadyTriggered;
    // Use this for initialization
    void Start()
    {
        alreadyTriggered = false;
        CanMove = false;
        //y = Water.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if(CanMove)
        {
            if(Water.transform.position.y>= (y-distanceToBeMoved))
            {
                Water.transform.Translate(Vector2.down * MovementSpeed * Time.deltaTime);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!alreadyTriggered)
        {
            y = Water.transform.position.y;
            if (collision.gameObject.tag == "Player")
            {
                CanMove = true;
                alreadyTriggered = true;
            }           
        }
    }
}
