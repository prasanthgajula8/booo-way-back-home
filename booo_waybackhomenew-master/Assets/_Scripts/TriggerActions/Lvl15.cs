﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lvl15 : MonoBehaviour
{
    public GameObject thisPlanet;
    public GameObject otherPlanet;
    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player")
        {
            thisPlanet.GetComponent<RotateAround>().Enable = false;
            otherPlanet.GetComponent<RotateAround>().Enable = true;
        }
    }
}
