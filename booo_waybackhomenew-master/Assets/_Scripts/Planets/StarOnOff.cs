﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarOnOff : MonoBehaviour
{
    bool On;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("OnOff", 0f, 3f);
    }

    // Update is called once per frame
    void Update()
    {
        if(!On)
        {
            this.GetComponent<CircleCollider2D>().radius = 0f;
            this.GetComponent<SpriteRenderer>().enabled = false;
        }

        else if(On)
        {
            this.GetComponent<CircleCollider2D>().radius = 0.36f;
            this.GetComponent<SpriteRenderer>().enabled = true;

        }
    }

    public void OnOff()
    {
        if(On)
        {
            On = false;
        }

        else if(!On)
        {
            On = true;
        }
    }
}
