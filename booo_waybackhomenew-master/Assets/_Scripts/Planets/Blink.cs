﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blink : MonoBehaviour
{
    public float a, b, c, d, e, f;
    // Use this for initialization
    void Start()
    {
        InvokeRepeating("StartLoop", 0f, 6f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeTransform(float a,float b)
    {
        transform.position = new Vector2(a, b);
    }

    IEnumerator loop()
    {
        ChangeTransform(a, b);
        yield return new WaitForSeconds(2f);
        ChangeTransform(c, d);
        yield return new WaitForSeconds(2f);
        ChangeTransform(e, f);
        yield return new WaitForSeconds(2f);
    }

    public void StartLoop()
    {
        StartCoroutine(loop());
    }
}
