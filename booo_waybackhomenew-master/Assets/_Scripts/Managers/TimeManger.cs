﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Khadga.UtilScripts.LocalData;

public class TimeManger : MonoBehaviour
{
    DateTime PreviousLogIn;
    DateTime CurrentLogin;
    int dur;

    private void Start()
    {
        SaveManager.Instance.Levels = DataManager.instance.gameData.Levels;
        PreviousLogIn = DataManager.instance.gameData.PreviousLogIn;
        CurrentLogin = System.DateTime.Now;
    
        dur = CurrentLogin.DayOfYear - PreviousLogIn.DayOfYear;
        CheckForDailyLogin();
    }

    public void CheckForDailyLogin()
    {
        if (dur>1)
        {
            AchivementManger.Instance.SetAchivemnt(AchivementManger.Instance.AchievementList[10]);
            DataManager.instance.gameData.PreviousLogIn = CurrentLogin;
            DataManager.instance.SaveData();
        }
        else if(dur==1)
        {
            AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList[10]);
            DataManager.instance.gameData.PreviousLogIn = CurrentLogin;
            DataManager.instance.SaveData();
        }
    }
   
}