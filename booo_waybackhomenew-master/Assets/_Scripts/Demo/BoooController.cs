﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class BoooController : MonoBehaviour
{
    #region AUDIO INPUT RELATED
    public float sensitivity = 100;
    public float loudness = 0;
    public float Treshold = 6;
    private bool IsAudioJump=false;
    AudioSource AudioSource;
    Slider SensitivitySlider;
    bool CanJump = true;
    bool JumpStarted;
    float MinJumpInterval = 0.5f;

    private void IntializeAudioInput()
    {

        SensitivitySlider = FindObjectOfType<Slider>();
        if (SensitivitySlider != null)
        {
            SensitivitySlider.minValue = 5;
            SensitivitySlider.maxValue = 120;
            SensitivitySlider.value = 80;
        }
        // setting microphone input to audio source and setting loop = true
        AudioSource.clip = Microphone.Start(null, true, 1, 44100);
        AudioSource.loop = true; // Set the AudioClip to loop
        //AudioSource.mute = true; // Mute the sound, we don't want the player to hear it
        while (!(Microphone.GetPosition(null) > 0)) { } // Wait until the recording has started
        AudioSource.Play(); // Play the audio source!
    }

    private void CheckAudioInput()
    {
            if (SensitivitySlider != null)
                sensitivity = SensitivitySlider.value;
            loudness = GetAveragedVolume() * sensitivity;

            if (loudness > Treshold)
            {
                if (IsFrontGrounded || IsBackGrounded || IsMiddleGrounded)
                {
                // Debug.Log(loudness+" ready to jump");
                if (CanJump)
                {
                    Jump();
                    if (MainMenu.Chapter_Index == 0)
                    {
                        if (AchivementManger.Instance != null)
                            AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList[0]);
                    }
                    else if (MainMenu.Chapter_Index == 1)
                    {
                        if (AchivementManger.Instance != null)
                            AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList2[0]);
                    }
                    IsAudioJump = true;
                    CanJump = false;
                }
                }
            }
        
    }

  
    IEnumerator SetCanJump()
    {
        yield return new WaitForSeconds(MinJumpInterval);
        CanJump = true;
    }

    float GetAveragedVolume()
    {
        float[] data = new float[256];
        float a = 0;
        AudioSource.GetOutputData(data, 0);
        foreach (float s in data)
        {
            a += Mathf.Abs(s);
        }
        return a / 256;
    }

    #endregion

    public Transform MiddleGroundCheck;
    public Transform BackGroundCheck;
    public Transform ForwardGroundCheck;

    public LayerMask WhatIsGround;
    public bool IsLookingRight = true;
    bool IsBackGrounded = false;
    bool IsFrontGrounded = false;
    bool IsMiddleGrounded = false;

    public Animator Anim;
    float VerticalVelocity;
    float HorizontalVelocity;

    InputManager InputManager;
    CorgiController CorgiController;
    // Use this for initialization
    void Start()
    {
        Anim = GetComponent<Animator>();
        CorgiController = GetComponent<CorgiController>();
        InputManager = FindObjectOfType<InputManager>();
        AudioSource = GetComponent<AudioSource>();
        IntializeAudioInput();
    }

    private void Update()
    {

        IsBackGrounded = Physics2D.OverlapCircle(BackGroundCheck.position, 0.15F, WhatIsGround);
        IsFrontGrounded = Physics2D.OverlapCircle(ForwardGroundCheck.position, 0.15F, WhatIsGround);
        IsMiddleGrounded = Physics2D.OverlapCircle(MiddleGroundCheck.position, 0.15F, WhatIsGround);

        if (IsFrontGrounded || IsBackGrounded || IsMiddleGrounded)
            CheckAudioInput();

        HorizontalVelocity = CorgiController.Speed.x;
        VerticalVelocity = CorgiController.Speed.y;


        Anim.SetFloat("Speed", Mathf.Abs(HorizontalVelocity));
        Anim.SetBool("IsGrounded", IsBackGrounded || IsFrontGrounded || IsMiddleGrounded);
        Anim.SetFloat("vSpeed", VerticalVelocity);


        if ((HorizontalVelocity > 0 && !IsLookingRight) || (HorizontalVelocity < 0 && IsLookingRight))
            Flip();

        if (JumpStarted == true)
        {
            StartCoroutine(SetCanJump());
            JumpStarted = false;
        }

    }

    //private void LateUpdate()
    //{
    //    //Checking audio input every frame
      
    //}


    //private void FixedUpdate1()
    //{
    //    IsBackGrounded = Physics2D.OverlapCircle(BackGroundCheck.position, 0.15F, WhatIsGround);
    //    IsFrontGrounded = Physics2D.OverlapCircle(ForwardGroundCheck.position, 0.15F, WhatIsGround);
    //    IsMiddleGrounded = Physics2D.OverlapCircle(MiddleGroundCheck.position, 0.15F, WhatIsGround);


    //    HorizontalVelocity = CorgiController.Speed.x;
    //    VerticalVelocity = CorgiController.Speed.y;


    //    Anim.SetFloat("Speed", Mathf.Abs(HorizontalVelocity));
    //    Anim.SetBool("IsGrounded", IsBackGrounded || IsFrontGrounded ||IsMiddleGrounded);
    //    Anim.SetFloat("vSpeed", VerticalVelocity);


    //    if ((HorizontalVelocity > 0 && !IsLookingRight) || (HorizontalVelocity < 0 && IsLookingRight))
    //        Flip();
    //}

    public void Flip()
    {
        IsLookingRight = !IsLookingRight;
        Vector3 myScale = transform.localScale;
        myScale.x *= -1;
        transform.localScale = myScale;
    }

    public void Jump()
    {
        //Debug.Log("Booo status " + IsBackGrounded);
        if (IsBackGrounded||IsFrontGrounded||IsMiddleGrounded)
        {
            JumpStarted = true;
                InputManager.JumpButtonDown();
                //Debug.Log("audio input success");
            
        }
    }
}
