﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonMono : MonoBehaviour {

    public static SingletonMono Instance;

    public int SampleInt;

    public void Awake()
    {
        if (Instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
}
