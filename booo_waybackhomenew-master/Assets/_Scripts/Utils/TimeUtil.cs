﻿using UnityEngine;
using System.Collections;
using System;

namespace Khadga.UtilScripts.Time
{
    public static class TimeUtil
    {
        public static DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public const int SECONDSINMINUTE = 60;

        public const int SECONDSINHOUR = 60 * SECONDSINMINUTE;

        public const int SECONDSINDAY = 24 * SECONDSINHOUR;

        public const int SECONDSINWEEK = 7 * SECONDSINDAY;

        public static long GetSecDifference(long previousEpochTime)
        {
            var timeSpanFromEpoch = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            long currentEpochTime = (long)timeSpanFromEpoch.TotalMilliseconds;

            Console.Out.WriteLine("Current datetiem.utcNow:" + DateTime.UtcNow);
            Console.Out.WriteLine("Current epoch time:" + currentEpochTime);
            Console.Out.WriteLine("previous EpochTime:" + previousEpochTime);
            return ((currentEpochTime - previousEpochTime) / (1000));
        }

        public static TimeSpan GetTimeSpanBetweenEpochs(long timePeriod1,long timePeriod2)
        {
            return (GetDateTimeFromEpoch(timePeriod1) - GetDateTimeFromEpoch(timePeriod2));
        }


        public static long GetCurrentEpoch()
        {
            var timeSpanFromEpoch = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            long currentEpochTime = (long)timeSpanFromEpoch.TotalMilliseconds;

            return currentEpochTime;
        }

        // Converts the epoch time to local time
        public static DateTime GetLocalTime(long epoch)
        {
            return Epoch.AddMilliseconds(epoch).ToLocalTime();
        }

        // converts the epoch time to DateTime
        public static DateTime GetDateTimeFromEpoch(long epoch)
        {
            return Epoch.AddMilliseconds(epoch);
        }


        // returns the short string message 
        // returns the time if the message is received before oneday
        // else returns  2 days ago/ 3 days ago
        // used for tasks which are completed
        public static string GetLocalTimeString(long epoch)
        {

            DateTime messageTimeStamp = Epoch.AddMilliseconds(epoch).ToLocalTime();

            var x = DateTime.Now.ToLocalTime();

            var y = messageTimeStamp;

            TimeSpan timeDiff = x - y;

            long secDiff = (long)timeDiff.TotalSeconds;

            int numOfSecPassedToday = (int)(DateTime.Now - DateTime.Today).TotalSeconds;

            if (secDiff < numOfSecPassedToday)
            {
                return messageTimeStamp.ToShortTimeString();
            }
            else if (secDiff < SECONDSINDAY * 2)
            {
                return "yesterday";
            }
            else if (secDiff < SECONDSINWEEK)
            {
                return (secDiff / SECONDSINDAY).ToString() + " days ago";
            }
            else
                return messageTimeStamp.ToShortDateString();

        }

        public static long GetEpochFromDateTime(DateTime dateTime)
        {
            var timeSpanFromEpoch = dateTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            long epochTime = (long)timeSpanFromEpoch.TotalMilliseconds;

            return epochTime;
        }


        // returns the short string like "2hr 25min" used for tasks which are not completed
        public static string GetRemainingTimeShotString(long epoch)
        {
            DateTime dateTime = GetLocalTime(epoch);

            TimeSpan remaingingTime = dateTime -DateTime.Now;

            if (remaingingTime.TotalSeconds > 0)
            {
                if (remaingingTime.TotalSeconds <= 60)
                {
                    return StringReturnFunction(remaingingTime.Seconds) + "sec";
                }
                else if (remaingingTime.TotalMinutes < 60)
                {
                    return StringReturnFunction(remaingingTime.Minutes) + "min " + StringReturnFunction(remaingingTime.Seconds) + "sec";
                }
                else if (remaingingTime.TotalHours < 24)
                {
                    return StringReturnFunction(remaingingTime.Hours) + "hr " + StringReturnFunction(remaingingTime.Minutes) + "min";
                }
                else
                    return "more than one day";
            }
            else
                return null;
        }

        public static int GetRemainingDays(long epoch)
        {
            DateTime dateTime = GetLocalTime(epoch);

            TimeSpan remaingingTime = dateTime-DateTime.Now;

            return (int)remaingingTime.TotalDays;
        }

        public static long GetRemainingSeconds(long epoch)
        {
            DateTime dateTime = GetLocalTime(epoch);

            TimeSpan remaingingTime = dateTime - DateTime.Now;

            return (long)remaingingTime.TotalSeconds;
        }

        // get remaining seconds from date time
        public static long GetRemainingSeconds(DateTime dateTime)
        {
            TimeSpan remaingingTime = dateTime - DateTime.Now;

            return (long)remaingingTime.TotalSeconds;
        }

        public static string StringReturnFunction(int Time)
        {
            if (Time < 10)
                return "0" + Time.ToString();
            else
                return Time.ToString();
        }
    }
}