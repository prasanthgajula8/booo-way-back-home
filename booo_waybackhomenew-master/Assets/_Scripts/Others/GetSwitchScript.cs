﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GetSwitchScript : MonoBehaviour
{
    public bool isCalled = false;
    public static GetSwitchScript Instance;
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else if (Instance == null)
        {
            Instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
    public GameObject SwitchButton;
    [HideInInspector]
    public GameObject InLevelSwitchButton;
    public void Fetch(GameObject SwitchRef)
    {
        InLevelSwitchButton = SwitchRef;
        InLevelSwitchButton.GetComponent<Button>().interactable = false;
        InLevelSwitchButton.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        SwitchButton.GetComponent<Button>().onClick.AddListener(callSwitch);
    }
    public void callSwitch()
    {

        if (!isCalled)
        {
            InLevelSwitchButton.GetComponent<Button>().onClick.Invoke();
            isCalled = true;
            Pause.Instance.Resume();
        }
    }
}
