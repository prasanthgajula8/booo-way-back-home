﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BooCoordinates : MonoBehaviour
{
    GameObject newText;
    private void Start()
    {
        newText = new GameObject("Displayer", typeof(RectTransform));
        var newTextComp = newText.AddComponent<Text>();
        newText.transform.SetParent(GameObject.FindObjectOfType<Canvas>().transform);
        newText.GetComponent<RectTransform>().sizeDelta = new Vector2(300, 300);
        newText.GetComponent<RectTransform>().position = new Vector3(0, 0, 0);
        newText.GetComponent<Text>().resizeTextForBestFit = true;
        newText.GetComponent<Text>().color = new Color(255, 0, 0);
        newText.GetComponent<Text>().font = GameObject.FindObjectOfType<Text>().font;

    }
    private void Update()
    {
        newText.GetComponent<Text>().text = (this.transform.position.x.ToString() +" , "+ this.transform.position.y.ToString());
        

    }
}
