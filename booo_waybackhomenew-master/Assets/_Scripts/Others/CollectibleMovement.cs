﻿using UnityEngine;
using System.Collections;

public class CollectibleMovement : MonoBehaviour
{
    public Transform target;
    float speed;
    bool canMove;
    public static bool ReachedDestination;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        if(canMove)
        {
            this.transform.parent.position = Vector3.MoveTowards(this.transform.parent.position, target.position,0.2f);
        }

        if(this.transform.parent.position==target.position)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player")
        {
            canMove = true;
        }
    }
}