﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchInitializer : MonoBehaviour
{
	void Start ()
    {
        Pause.Instance.SwitchControlsButton.GetComponent<GetSwitchScript>().Fetch(this.gameObject);
	}
}
