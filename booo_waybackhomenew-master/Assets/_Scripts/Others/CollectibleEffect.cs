﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleEffect : MonoBehaviour
{
    public GameObject Effect;
    int count = 1;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Soul")
        {
            Instantiate(Effect, transform.position, transform.rotation);
        }
    }
}
