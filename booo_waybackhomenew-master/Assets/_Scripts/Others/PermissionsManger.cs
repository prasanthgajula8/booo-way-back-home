﻿using System.Collections;
using UnityEngine;

public class  PermissionsManger : MonoBehaviour
{
    public Splash Splash;
    private const string MICROPHONE_PERMISSION = "android.permission.RECORD_AUDIO";
    public static PermissionsManger instance;
    public void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else if (instance == null)
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
  
    private void Start()
    {

    }
 
    public void ChecKForPermissions()
    {
        if (!CheckPermissions())
        {
            AskForPermissions();
            Debug.LogWarning("Missing permission to access micro phone, please grant the permission first");
        }
        else
        {
            Splash.IsGrantedAccess = true;
        }
   }

    private bool CheckPermissions()
    {
        if (Application.platform != RuntimePlatform.Android)
        {
            Splash.IsGrantedAccess = true;
            return true;
        }

        return AndroidPermissionsManager.IsPermissionGranted(MICROPHONE_PERMISSION);
    }

    public void AskForPermissions()
    {
        AndroidPermissionsManager.RequestPermission(new[] { MICROPHONE_PERMISSION }, new AndroidPermissionCallback(
            grantedPermission =>
            {
                Splash.IsGrantedAccess = true;
            },
            deniedPermission =>
            {
                Splash.IsDeninedAccess = true;
            }));
    }

   
    
}
