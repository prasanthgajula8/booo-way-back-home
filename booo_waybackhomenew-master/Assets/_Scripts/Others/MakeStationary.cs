﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeStationary : MonoBehaviour
{
    public bool one;
    public bool two;
    public bool three;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player")
        {
            this.GetComponentInParent<CircularMovement>().degreesPerSecond = 0f;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if(one)
            {
                this.GetComponentInParent<CircularMovement>().degreesPerSecond = -65f;
            }

            if (two)
            {
                this.GetComponentInParent<CircularMovement>().degreesPerSecond = -30f;
            }

            if (three)
            {
                this.GetComponentInParent<CircularMovement>().degreesPerSecond = -25f;
            }
        }
    }
}
