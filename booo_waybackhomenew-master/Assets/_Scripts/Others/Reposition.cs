﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class Reposition : MonoBehaviour
{
    public GameObject RepPrefab;
    Vector3 Position;
    // Use this for initialization
    void Start()
    {
        Position = this.transform.position;
    }

    public void OnEnable()
    {
        Health.CharacterDead += OnDeath;
    }

    public void OnDisable()
    {
        Health.CharacterDead -= OnDeath;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnDeath()
    {
        Instantiate(RepPrefab, Position, Quaternion.identity);
        Destroy(this.gameObject);

    }
}
