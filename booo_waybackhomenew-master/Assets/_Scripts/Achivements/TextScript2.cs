﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextScript2 : MonoBehaviour {

    public Text[] Achievement_Text = new Text[10];
    public Image[] RedLines = new Image[10];
    private static TextScript2 instance;
    // Game Instance Singleton
    public static TextScript2 Instance
    {
        get
        {
            return instance;
        }
    }

    public void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else if (instance == null)
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
    // Use this for initialization
    void Start () {

        Initialize();
	}

   public void Initialize()
    {
        int i;
        for ( i = 0; i < 10; i++)
        {
            Achievement_Text[i].text = AchivementManger.Instance.AchievementList2[i].Prefix + AchivementManger.Instance.AchievementList2[i].Sufix;
        }

        for ( i = 0; i < 10; i++)
        {
            if (AchivementManger.Instance.AchievementList2[i].IsCompleted)
                    RedLines[i].enabled = true;
                else
                    RedLines[i].enabled = false;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
