﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextScript : MonoBehaviour {

    public Text[] Achievement_Text = new Text[11];
    public Image[] RedLines = new Image[11];
    private static TextScript instance;
    // Game Instance Singleton
    public static TextScript Instance
    {
        get
        {
            return instance;
        }
    }

    public void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else if (instance == null)
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
    // Use this for initialization
    void Start () {

        Initialize();
	}

  public  void Initialize()
    {
        int i;
        for ( i = 0; i < 11; i++)
        {
            Achievement_Text[i].text = AchivementManger.Instance.AchievementList[i].Prefix + AchivementManger.Instance.AchievementList[i].Sufix;
        }

        for ( i = 0; i < 11; i++)
        {
            if (AchivementManger.Instance.AchievementList[i].IsCompleted)
                    RedLines[i].enabled = true;
                else
                    RedLines[i].enabled = false;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
