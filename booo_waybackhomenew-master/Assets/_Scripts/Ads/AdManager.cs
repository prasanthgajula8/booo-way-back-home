﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour
{
    [SerializeField] string gameID = "1703879";

    void Awake()
    {
        Advertisement.Initialize(gameID, true);
    }

    public void ShowAd()
    {
        Debug.Log("Button clicked");
#if UNITY_EDITOR
        StartCoroutine(WaitForAd());
#endif

        ShowOptions options = new ShowOptions();
        options.resultCallback = AdCallbackhandler;

        if (Advertisement.IsReady())
            Advertisement.Show(options);
    }

    void AdCallbackhandler(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                {
                    AchivementManger.Instance.UnlockAchivement(AchivementManger.Instance.CurrentAchivementIndex, AchivementManger.Instance.CurrentChapterNo);
                    Debug.Log("Ad Finished. Rewarding player...");
                    break;
                }
            case ShowResult.Skipped:
                Debug.Log("Ad skipped. Son, I am dissapointed in you");
                break;
            case ShowResult.Failed:
                Debug.Log("I swear this has never happened to me before");
                break;
        }
    }

    IEnumerator WaitForAd()
    {
        float currentTimeScale = Time.timeScale;
        Time.timeScale = 0f;
        yield return null;

        while (Advertisement.isShowing)
            yield return null;

        Time.timeScale = currentTimeScale;
    }
}