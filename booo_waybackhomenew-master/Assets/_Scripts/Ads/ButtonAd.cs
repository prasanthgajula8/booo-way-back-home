﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonAd : MonoBehaviour {

    public int AchivementIndex;
    public int ChapterNo;

   public void OnButtonClick()
    {
        AchivementManger.Instance.CurrentAchivementIndex = this.AchivementIndex;
        AchivementManger.Instance.CurrentChapterNo =this.ChapterNo;
    }
}
