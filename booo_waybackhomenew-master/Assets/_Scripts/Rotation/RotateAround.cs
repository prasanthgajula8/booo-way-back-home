﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : MonoBehaviour
{
    public Transform center;
    public float degreesPerSecond = -65.0f;
    public bool Enable = false;
    private Vector3 v;
    // Use this for initialization
    void Start()
    {
        v = transform.position - center.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Enable)
        {
            transform.RotateAround(center.transform.position, Vector3.forward, degreesPerSecond);
        }
    }
}
