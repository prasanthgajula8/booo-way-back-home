﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;
using Khadga.UtilScripts.LocalData;
using Khadga.FreakSpace;

namespace MoreMountains.CorgiEngine
{
    public class NextLevel : MonoBehaviour
    {
        public GameObject InfoPopUp;
        public bool isLazer;
        bool IsTriggered=false;
        // Use this for initialization
        void Start()
        {
            if(DataManager.instance.gameData.IsTutorialPlayed==false)
            InfoPopUp.SetActive(false);
            if(PlayerManager.Instance!= null&&DataManager.instance.gameData.IsTutorialPlayed == true)
            PlayerManager.Instance.LoadingPanel.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
      
        }

        //updates the current level status with acievements 
        void Update_LevelStatus()
        {
            int i;
            i = MainMenu.Level_Index;
            int count = PlayerManager.Instance.DisplayStars();
            if(SaveManager.Instance.Levels[i+1]==-1)
                SaveManager.Instance.Levels[i+1] = 0;
            if (count == 0)
            {
                if (SaveManager.Instance.Levels[i] <= 0)
                    SaveManager.Instance.Levels[i] = 0;
            }
            else if (count == 1)
            {
                if (SaveManager.Instance.Levels[i] <= 1)
                    SaveManager.Instance.Levels[i] = 1;
            }
            else if (count == 2)
            {
                if (SaveManager.Instance.Levels[i] <= 2)
                    SaveManager.Instance.Levels[i] = 2;
            }
            else if (count == 3)
                SaveManager.Instance.Levels[i] = 3;
               SaveManager.Instance.SaveData();
        }

        //Sets the Booo false
        void SetBooFalse()
        {
            GameObject.FindWithTag("Player").SetActive(false);
        }

        //Displays Player Statistics
        void Diplay_Statistics()
        {
            GameEndPopUp.Instance.TimeTaken.text = " " + Mathf.Ceil( PlayerManager.Instance.Timetaken);
            GameEndPopUp.Instance.ReviveCount.text = " " + PlayerManager.Instance.ReviveCount;
            GameEndPopUp.Instance.SoulsToBeColllected.text = "" + SoulsConstraint.SoulsToBeCollected;
        }

        void Check_Achievements()
        {
            if (MainMenu.Chapter_Index == 0)
            {
                if (PlayerManager.Instance.ReviveCount == 0)
                    AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList[7]);
                if (SoulManager.SoulsCollected == 0)
                    AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList[8]);
                if (MainMenu.Level_Index == 10)
                    AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList[6]);
            }
            else if (MainMenu.Chapter_Index == 1)
            {
                if (PlayerManager.Instance.ReviveCount == 0)
                    AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList2[7]);
                if (SoulManager.SoulsCollected == 0)
                    AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList2[8]);
                if (MainMenu.Level_Index == 10)
                    AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList2[6]);
            }
        }

        //Sets the UI in required manner
        void Adjust_UI()
        {
            if (MainMenu.Level_Index == 10 || MainMenu.Level_Index == 20 || MainMenu.Level_Index == 35)
                GameEndPopUp.Instance.nextlevel.SetActive(false);
            else
            GameEndPopUp.Instance.gameoverpanel.SetActive(true);
            Pause.Instance.pauseButton.SetActive(false);
            Pause.Instance.SoulsPanel.SetActive(false);
            GameObject.Find("Canvas").SetActive(false);
        }

        void ResetData()
        {
            PlayerManager.Instance.StarCount = 0;
            PlayerManager.Instance.ReviveCount = 0;
        }

       void AdjustUI_Demo()
        {
            GameEndPopUp.Instance.gameoverpanel2.SetActive(true);
            Pause.Instance.pauseButton.SetActive(false);
            GameObject.Find("Canvas").SetActive(false);
        }

        public void LoadMainMenu()
        {
            SceneManager.LoadScene("MainMenu");
        }

        //Displays GameOver Popup When Booo Touches Portal
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                if (!IsTriggered)
                {
                    IsTriggered = true;
                    Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[3].Name);
                    SetBooFalse();
                    if (SaveManager.Instance != null)
                        SaveManager.Instance.SaveData();
                    if (DataManager.instance.gameData.IsTutorialPlayed != true)
                    {
                        DataManager.instance.gameData.IsTutorialPlayed = true;
                        DataManager.instance.SaveData();
                        InfoPopUp.SetActive(true);
                    }
                    if (MainMenu.Level_Index != -1 && MainMenu.Level_Index != -2 && MainMenu.Level_Index != -3)
                    {
                        Adjust_UI();
                        Diplay_Statistics();
                        Check_Achievements();
                        Update_LevelStatus();
                    }
                    else if (MainMenu.Level_Index == -1 || MainMenu.Level_Index == -2 || MainMenu.Level_Index == -3)
                    {
                        AdjustUI_Demo();
                    }
                    ResetData();
                }
            }
        }
    }
}
