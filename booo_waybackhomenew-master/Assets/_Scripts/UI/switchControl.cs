﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class switchControl : MonoBehaviour
{
    public GameObject NormalPanel;
    public GameObject VoicePanel;
    public bool NormalbyVoice;
    BoooController Controller;
    int Switch;
    // Use this for initialization
    void Start()
    {
        Switch = DataManager.instance.gameData.Switch;
        FindObjectOfType<BoooController>().GetComponent<AudioSource>().volume = 1;
        if (Switch == 1)
        {
            VoicePanel.SetActive(false);
            NormalPanel.SetActive(true);
            FindObjectOfType<BoooController>().GetComponent<AudioSource>().volume = 0;
        }

        if (Switch == 0)
        {
            NormalPanel.SetActive(false);
            VoicePanel.SetActive(true);
            FindObjectOfType<BoooController>().GetComponent<AudioSource>().volume = 1;
        }
        // AudioListener.pause = false;
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    public void ChangeControl()
    {
        StartCoroutine(holdTime(0.5f));//edit by akshay
        //if(AudioListener.pause)
        //{
        //    AudioListener.pause = false;
        //}
        //else if(!AudioListener.pause)
        //{
        //    AudioListener.pause = true;
        //}
        if(NormalPanel.activeSelf==true)
        {
            DataManager.instance.gameData.Switch = 0;
            DataManager.instance.SaveData();
            NormalPanel.SetActive(false);
            VoicePanel.SetActive(true);
            FindObjectOfType<BoooController>().GetComponent<AudioSource>().volume = 1;
        }
        else if(VoicePanel.activeSelf==true)
        {
            DataManager.instance.gameData.Switch = 1;
            DataManager.instance.SaveData();
            VoicePanel.SetActive(false);
            NormalPanel.SetActive(true);
            FindObjectOfType<BoooController>().GetComponent<AudioSource>().volume = 0;

        }
    }

    IEnumerator holdTime(float time)//edit by akshay
    {
        yield return new WaitForSecondsRealtime(time);
        GetSwitchScript.Instance.isCalled = false;
    }
    public void Jump()
    {
        if(Controller!=null)
        {
            Controller.Jump();
        }

        else
        {
            Controller = FindObjectOfType<BoooController>();
            Controller.Jump();
        }
    }
}
