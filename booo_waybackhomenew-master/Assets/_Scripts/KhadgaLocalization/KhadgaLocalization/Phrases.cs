﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Khadga.Localization
{
    [Serializable]
    public class Phrases
    {
        public string Name;
        public string Description;
        public string Number;
        public string UnlockAll;
        public string PurchaseAll;
        public string BewareOfGravity;
        public string Gravity;
        public string FirstSteps;
        public string CelestialBodies;
        public string Switch;
        public string Back;
        public string NextLevel;
        public string Options;
        public string Sensitivity;
        public string Menu;
        public string Souls;
        public string HoldHereToMoveLeft;
        public string HoldHereToMoveRight;
        public string AdjustSensitivity;
        public string CollectSouls;
        public string Ok;
        public string Mute;
        public string UnMute;
        public string Demo;
        public string Language;
        public string English;
        public string Chinese;
        public string Japanese;
        public string Spanish;
        public string Arabic;
        public string Buy;
        public string Retry;
        public string TimeTaken;
        public string LivesTakenToComplete;
        public string LevelsCompleted;
        public string SoulsCompleted;
        public string Resume;
        public string Restart;
        public string GivePermissions;
        public string Pause;
        public string Share;
        public string NoAds;
        public string UnlimitedPlaytime;
        public string Paused;
        public string Loading;
        public string SwitchControls;
        public string UnLockAllChapters;
        public string GiveAudioPermissionsToEnjoyTheGame;
        public string ClickOntheAboveButtonToSwitchControls;
        public string I_AmAwsome;
        public string I_AmNot;
     
        //public Phrases(string name, string description, string number)
        //{
        //    Name = name;
        //    Description = description;
        //    Number = number;
        //}

    }
}