﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAP_Manager: MainMenu {

    public Animator anim2, anim3;
   
	// Use this for initialization
	void Start () {
        checkforIAP();
	}
	
	// Update is called once per frame
	void Update () {
        if (AchivementManger.Achievements_Completed)
            Unlock_Chapter2();
	}

    public void Chapter2_anim_trigger()
    {
        anim2.SetTrigger(MenuConstants.CH2_ANIM_FRWD);
    }

    public void Chapter3_anim_trigger()
    {
        anim3.SetTrigger(MenuConstants.CH2_ANIM_FRWD);
    }

    public void Unlock_Chapter2()
    {
        DataManager.instance.gameData.IsChapterTwoUnclocked = true;
        DataManager.instance.SaveData();
        Unlock( ch2, Ch2_Ul);
        checkforIAP();
    }

    public void Unlock_Chapter3()
    {
        DataManager.instance.gameData.IsChapterThreeUnlocked = true;
        DataManager.instance.SaveData();
        Unlock( ch3, Ch3_Ul);
        checkforIAP();
    }

    public void Unlock_AllChapters()
    {
        DataManager.instance.gameData.IsAllChapterUnlocked = true;
        DataManager.instance.gameData.IsChapterThreeUnlocked = true;
         DataManager.instance.gameData.IsChapterTwoUnclocked = true;
        DataManager.instance.SaveData();
        checkforIAP();
    }

     void Unlock(GameObject obj1,GameObject obj2)
    {
        obj1.SetActive(false);
        obj2.SetActive(true);
    }

    public void back()
    {
        checkforIAP();
        if (Chapter_Index == 1)
        {
            anim2.SetTrigger(MenuConstants.CH2_ANIM_BKWD);
        }
        else if (Chapter_Index == 2)
        {
            anim3.SetTrigger(MenuConstants.CH2_ANIM_BKWD);
        }
    }

}