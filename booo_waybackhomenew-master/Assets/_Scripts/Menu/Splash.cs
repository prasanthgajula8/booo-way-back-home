﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Khadga.FreakSpace;
using Khadga.Localization;
using Khadga.UtilScripts.LocalData;
public class Splash : MonoBehaviour
{
    public GameObject PermissionsPopup;
    public Text LoadingText;
    public float Logotime = 2f;
    public bool IsGrantedAccess;
    public bool IsDeninedAccess;
    public GameObject SplashScreen;

    public void Loading()
    {
        LoadingText.enabled = true;
        bool IsTutorialCompleted = DataManager.instance.gameData.IsTutorialPlayed;
         if (IsTutorialCompleted == true)
          {
             SceneManager.LoadScene(MenuConstants.MAINMENU_SCENENAME);
          }
        else
        {
          SceneManager.LoadSceneAsync("Tutorial");
        }
    }

    // Use this for initialization
    void Start()
    {
        // PlayerPrefs.DeleteAll();
        LocalizationManager.SetLanguages();
       
        SoundManager.Instance.PlayMusic(SoundManager.Instance.GameSounds[0]);
        StartCoroutine(ShowLogo());
        LoadingText.enabled = false;

    }

    IEnumerator ShowLogo()
    {
        yield return new WaitForSeconds(Logotime);
        PermissionsManger.instance.ChecKForPermissions();
       
    }

    // Update is called once per frame
    void Update()
    {
        if (IsGrantedAccess)
        {
            IsGrantedAccess = false;
            Loading();
      
        }
        if (IsDeninedAccess)
        {
            IsDeninedAccess = false;
            ShowPopup();
        }

    }
    public void UserPermissionsrequired()
    {
        PermissionsManger.instance.AskForPermissions();
    }

    void ShowPopup()
    {
        SplashScreen.SetActive(false);
        PermissionsPopup.SetActive(true);
    }

}
