﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MoreMountains.CorgiEngine;
using Khadga.FreakSpace;

public class Pause : MonoBehaviour {
    public GameObject Pause_Mute_Button,Pause_UnMute_Button;
    public GameObject PauseMenu;
    public GameObject pauseButton;
    public GameObject SoulsPanel;
    public GameObject SwitchControlsButton;//edit by akshay
    private static Pause instance;

    // Game Instance Singleton
    public static Pause Instance
    {
        get
        {
            return instance;
        }
    }

    public void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else if (instance == null)
        {
            instance = this;
        }
       DontDestroyOnLoad(this.gameObject);
    }

    // Use this for initialization
    void Start () {

        Initialize();
	}

    void Initialize()
    {
        SoulsPanel.SetActive(false);
        pauseButton.SetActive(false);
        PauseMenu.SetActive(false);
        Pause_Mute_Button.SetActive(true);
        Pause_UnMute_Button.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    //Pauses the gameplay
    public void pause()
    {
        SaveManager.Instance.SavePlayerData();
        Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[3].Name);
        PlayerManager.Instance.Level_name.gameObject.SetActive(false);
        Adjust_UI(0, true, false,false);
    }

    //Continues the gameplay
    public void Resume()
    {
        Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[5].Name);
        Adjust_UI(1, false, true,true);
    }

    //Restarts the gameplay
    public void Restart()
    {
        Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[5].Name);
        Adjust_UI(1, false, true,true);
        PlayerManager.Instance.Timetaken = 0;
        PlayerManager.Instance.ReviveCount = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    //Adjusts the UI
    void Adjust_UI(int val,bool b,bool b1,bool b2)
    {
        Time.timeScale = val;
        PauseMenu.SetActive(b);
        pauseButton.SetActive(b1);
        SoulsPanel.SetActive(b2);
    }

    //Loads MainMenu
    public void LoadMainMenu()
    {   
        Time.timeScale = 1;
        PlayerManager.Instance.ReviveCount = 0;
        GameEndPopUp.Instance.gameoverpanel.SetActive(false);
        Pause.Instance.PauseMenu.SetActive(false);
        StartCoroutine(PlayerManager.Instance.Async_Operation(MenuConstants.MAINMENU_SCENENAME,MenuConstants.MAINMENU_SCENENAME));
    }

    void Mute_Unmute(bool b1,bool b2)
    {
        Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[3].Name);
        Khadga.FreakSpace.SoundManager.Instance.ToggleMute();
        Pause_Mute_Button.SetActive(b1);
        Pause_UnMute_Button.SetActive(b2);
    }

    public void PauseMute()
    {
        Mute_Unmute(false, true);
    }
    public void PauseUnMute()
    {
        Mute_Unmute(true, false);
    }

}
