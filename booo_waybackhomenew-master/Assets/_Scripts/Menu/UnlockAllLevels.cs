﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockAllLevels : MonoBehaviour {

    public GameObject unlockLevels_panel;

	// Use this for initialization
	void Start () {
        unlockLevels_panel.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Unlockalllevels()
    {
        Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[3].Name);
        GameObject.Find("Menu").SetActive(false);
        unlockLevels_panel.SetActive(true);
    }
}
