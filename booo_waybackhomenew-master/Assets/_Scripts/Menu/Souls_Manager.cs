﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.CorgiEngine;

public class Souls_Manager : MonoBehaviour {

    public Text Souls_Collected_Level,Souls_Collected_GEP,TotalSouls;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        TotalSouls.text = "" + SoulsConstraint.SoulsToBeCollected;
        Souls_Collected_Level.text = "" + SoulManager.SoulsCollected;
        Souls_Collected_GEP.text = "" + SoulManager.SoulsCollected;
    }

  }
