﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class ChapterSelectionManager : MonoBehaviour
{

    public GameObject LoadingPopup;
    public HorizontalScrollSnap HorizontalScrollSnap;
    public Animator ChapterAppearance;
    public List<GameObject> LevelPanels;

  

    public void OnChapterClick(int chapterNumber)
    {
        if (HorizontalScrollSnap.CurrentPage == chapterNumber)
        {
            HideAllLevelPanels();
            Debug.Log("Open chapter " + chapterNumber);
            LevelPanels[chapterNumber].SetActive(true);
        }
        else
        {
            if (chapterNumber > HorizontalScrollSnap.CurrentPage)
                HorizontalScrollSnap.NextScreen();
            else
                HorizontalScrollSnap.PreviousScreen();
            Debug.Log("Snap to chapter " + chapterNumber);
        }

    }
  

    void HideAllLevelPanels()
    {
        foreach (var levelPanel in LevelPanels)
        {
            levelPanel.SetActive(false);
        }
    }

    public void OnBackClick()
    {
        HideAllLevelPanels();
    }

}
