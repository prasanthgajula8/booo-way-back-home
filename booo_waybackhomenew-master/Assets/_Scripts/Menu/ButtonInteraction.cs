﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.CorgiEngine;
using Newtonsoft.Json;
using Khadga.UtilScripts.LocalData;

public class ButtonInteraction
    : MonoBehaviour {

    public Button button;
    public Image star1, star2, star3;
    public int LevelIndex;
    [HideInInspector]
    public Sprite OnInteractable;//edit by akshay
    [HideInInspector]
    public GameObject levelNumber;
    public Sprite OnNotInteractable;//edit by akshay

	// Use this for initialization
	void Start() {
        OnInteractable = this.gameObject.GetComponent<Button>().targetGraphic.GetComponent<Image>().sprite;//edit by akshay
        levelNumber = this.gameObject.GetComponent<Button>().targetGraphic.transform.GetChild(0).gameObject;//edit by akshay

      // SaveManager.Levels = LocalDataUtil.GetLocalDataOfType<int[]>("LevelInfo");
        int i=LevelIndex;
        if (SaveManager.Instance.Levels[i] == -1)
        {
            button.interactable = false;
            EnableStars(false, false, false);
            this.gameObject.GetComponent<Button>().targetGraphic.GetComponent<Image>().sprite = OnNotInteractable;//edit by akshay
            levelNumber.SetActive(false);//edit by akshay
        }
       else if (SaveManager.Instance.Levels[i] == 0)
        {
            button.interactable = true;
            EnableStars(false, false, false);
            this.gameObject.GetComponent<Button>().targetGraphic.GetComponent<Image>().sprite = OnInteractable;//edit by akshay
            levelNumber.SetActive(true);//edit by akshay
        }
       else if (SaveManager.Instance.Levels[i] == 1)
        {
            button.interactable = true;
            EnableStars(true, false, false);
            this.gameObject.GetComponent<Button>().targetGraphic.GetComponent<Image>().sprite = OnInteractable;//edit by akshay
            levelNumber.SetActive(true);//edit by akshay
        }
        else if (SaveManager.Instance.Levels[i] == 2)
        {
            button.interactable = true;
            EnableStars(true,true, false);
            this.gameObject.GetComponent<Button>().targetGraphic.GetComponent<Image>().sprite = OnInteractable;//edit by akshay
            levelNumber.SetActive(true);//edit by akshay
        }
        else if (SaveManager.Instance.Levels[i] == 3)
        {
            button.interactable = true;
            EnableStars(true,true,true);
            this.gameObject.GetComponent<Button>().targetGraphic.GetComponent<Image>().sprite = OnInteractable;//edit by akshay
            levelNumber.SetActive(true);//edit by akshay
        }

    }

    void EnableStars(bool Bool1,bool Bool2,bool Bool3)
    {
        star3.enabled = Bool1;
        star2.enabled = Bool2;
        star1.enabled = Bool3;
    }

    // Update is called once per frame
    void Update () {
         
       
    }
}
